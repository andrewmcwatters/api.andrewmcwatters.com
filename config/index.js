/**
 * @license api.andrewmcwatters.com
 * (c) 2014 Andrew McWatters.
 */
module.exports = require('./' + (process.env.NODE_ENV || 'development') + '.json');
