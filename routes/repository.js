/**
 * @license api.andrewmcwatters.com
 * (c) 2014 Andrew McWatters.
 */
'use strict';

var app        = require('../app');
var passport   = require('passport');
var repository = require('../lib/repository');

app.post('/repository/:name',
  passport.authenticate('basic', { session: false }),
  function (req, res) {
    var name = req.params.name;

    if (!name) {
      res.sendStatus(400);
      return;
    }

    repository.onPostReceive(name, function (err) {
      if (err) {
        switch (err.message) {
        case 'Not Found':
          res.sendStatus(404);
          break;
        }
        return;
      }

      res.sendStatus(200);
    });
  }
);
